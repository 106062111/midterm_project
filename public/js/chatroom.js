function init() {
    var signed_out = false;
    var user_key;
    firebase.auth().onAuthStateChanged(function(user) {
        if(user == null && !signed_out){
            // alert("Sign In and start to CHATatNIGHT!");
            document.location = "index.html";
        }
        else{
            if(user){
                user_name = user.displayName;
                var user_email = user.email;
            }
            var txtDropdown = document.getElementById('navbarDropdownMenuLink');
            txtDropdown.innerText = user_name + " (" + user_email + ")";

            var btnSignOut = document.getElementById('btnSignOut');
            btnSignOut.addEventListener('click', function() {
                
                firebase.auth().signOut().then(function() {
                    signed_out = true;
                    firebase.database().ref('users').once('value', function(snapshot) {
                        snapshot.forEach(function(s) {
                            if(s.val().name == user_name){
                                user_key = s.key;
                                console.log(user_key);
                            }
                        });
                        if(user_key!="undefined"){
                            firebase.database().ref('users/'+user_key+'/online').set(false).then(function() {
                                document.location = "index.html";
                            });
                        }
                        else{
                            document.location = "index.html";
                        }
                    });
                    // alert("success");
                    // Sign-out successful.
                }).catch(function(error) {
                    alert("Please try again!");
                    // An error happened.
                });
                
            });
        }
    });
    
    var html_before_user_div_id = "<div id='";
    var html_after_user_div_id = "' class='new_user p-2' onclick='user_list_onclick_callback(id)'>";
    var html_end_user_div = "</div>";
    var new_user_list = document.getElementById('new_user_list');
    var online_list = document.getElementById('online_user_list');
    var user_list = document.getElementById('user_list');
    var html_user_list = [];
    var html_online_list = [];
    var html_new_user_list = [];
    var first_count = 0;
    var second_count = 0;

    firebase.database().ref('users').limitToLast(5).once("value", function(snapshot) {
        snapshot.forEach(function(s) {
            html_new_user_list.push(html_before_user_div_id + s.val().name + html_after_user_div_id + s.val().name + html_end_user_div);
            new_user_list.innerHTML = html_new_user_list.join("");
            // document.getElementById('new_user_list_'+s.val().name).addEventListener('click', user_list_onclick_callback(s.val().name));
            first_count += 1;
        });
        firebase.database().ref('users').limitToLast(5).on('child_added', function(s) {
            second_count += 1;
            if(second_count > first_count){
                html_new_user_list.shift();
                html_new_user_list.push(html_before_user_div_id + s.val().name + html_after_user_div_id + s.val().name + html_end_user_div);
                new_user_list.innerHTML = html_new_user_list.join("");
                // document.getElementById('new_user_list_'+s.val().name).addEventListener('click', user_list_onclick_callback(s.val().name));
                first_count += 1;
            }
        });
    });

    firebase.database().ref('users').orderByChild('online').equalTo(true).on('value', function(s) {
        html_online_list.length = 0;
        s.forEach(function(s) {
            html_online_list.push(html_before_user_div_id + s.val().name + html_after_user_div_id + s.val().name + html_end_user_div);
            online_list.innerHTML = html_online_list.join("");
        });
    });

    firebase.database().ref('users').orderByChild('online').equalTo(false).on('value', function(s) {
        html_user_list.length = 0;
        s.forEach(function(s) {
            html_user_list.push(html_before_user_div_id + s.val().name + html_after_user_div_id + s.val().name + html_end_user_div);
            user_list.innerHTML = html_user_list.join("");
        });
    });

    document.getElementById('btnPublicRoom').addEventListener('click', open_public_room);
}

var user_name;  // current signed in user

function user_list_onclick_callback(clicked_user_name) {

    // OFF the current chatlog listener
    firebase.database().ref('public').off();
    firebase.database().ref('chatlog/'+chatlog_key).off();

    draw_chatroom(clicked_user_name, "private");

    // send query to database to find the chatlog
    var ref = firebase.database().ref('userChatWith/'+user_name);
    var ref_chatlog = firebase.database().ref('chatlog');
    ref.orderByKey().equalTo(clicked_user_name).once("value", function(s) {
        if(!s.exists()){
            // console.log(clicked_user_name ,"DNE");
            // create a empty chat log and link its id under both users' chatwith_data
            chatlog_key = ref_chatlog.push().key;
            // document.getElementById('btnSend').addEventListener('click', send_chatmsg_to(chatlog_key));
            ref.child(clicked_user_name).set(chatlog_key);
            ref.parent.child(clicked_user_name).child(user_name).set(chatlog_key);
            turnON_update_chatlog();
        }
        else{
            // console.log(user_name, clicked_user_name);
            chatlog_key = s.child(clicked_user_name).val();
            // console.log(chatlog_key);
            // document.getElementById('btnSend').addEventListener('click', send_chatmsg_to(chatlog_key));
            ref_chatlog.orderByKey().equalTo(chatlog_key).once('value', function(s) {
                if(s.exists()){
                    // load the chat log and ON
                    load_chatlog_andON(s.child(chatlog_key));
                }
                else{
                    turnON_update_chatlog();
                }
            });
        }
    });
}

var chatlog_key;

function send_chatmsg() {
    var msg_content = document.getElementById('txtMsg');
    // console.log("btn pressed", chatlog_key);
    var StrippedString = msg_content.value.replace(/(<([^>]+)>)/ig,"");
    if(StrippedString != ""){
        var ref = firebase.database().ref('chatlog/'+chatlog_key);
        ref.push({
            sender: user_name,
            content: StrippedString
        });
        msg_content.value = "";
    }
}
function send_chatmsg_to_public() {
    var msg_content = document.getElementById('txtMsg');
    var StrippedString = msg_content.value.replace(/(<([^>]+)>)/ig,"");
    if(StrippedString != ""){
        var ref = firebase.database().ref('public');
        ref.push({
            sender: user_name,
            content: StrippedString
        });
        msg_content.value = "";
    }
}

function load_chatlog_andON(log_snapshot) {
    var html_before_content = "<div class='row w-100 px-2 mx-auto my-2'><div id='msg' class='col-sm-auto col-auto p-2 rounded bg-light'>";
    var html_myself_before_content = "<div class='row w-100 px-2 mx-auto my-2'><div class='col'></div><div id='msg_myself' class='col-sm-auto col-auto p-2 rounded'>";
    var html_end = "</div><div class='col'></div></div>";
    var html_myself_end = "</div></div>";
    var html_msgs = [];
    var msg_area = document.getElementById('msg_list');
    var first_count = 0;
    var second_count = 0;
    var log_key = log_snapshot.key;
    var sender;
    var content;
    log_snapshot.forEach(function(msg_snapshot) {
        sender = msg_snapshot.val().sender;
        content = msg_snapshot.val().content;
        if(sender == user_name){
            html_msgs.push(html_myself_before_content + content + html_myself_end);
        }
        else{
            html_msgs.push(html_before_content + content + html_end);
        }
        first_count += 1;
        msg_area.innerHTML = html_msgs.join("");
        msg_area.scrollTop = msg_area.scrollHeight;
    });
    firebase.database().ref('chatlog/'+log_key).on("child_added", function(s) {
        second_count += 1;
        if(second_count > first_count){
            sender = s.val().sender;
            content = s.val().content;
            if(sender == user_name){
                html_msgs.push(html_myself_before_content + content + html_myself_end);
            }
            else{
                html_msgs.push(html_before_content + content + html_end);
            }
            first_count += 1;
            msg_area.innerHTML = html_msgs.join("");
            msg_area.scrollTop = msg_area.scrollHeight;
        }
    });
}

function turnON_update_chatlog() {
    var html_before_content = "<div class='row w-100 px-2 mx-auto my-2'><div id='msg' class='col-sm-auto col-auto p-2 rounded bg-light'>";
    var html_myself_before_content = "<div class='row w-100 px-2 mx-auto my-2'><div class='col'></div><div id='msg_myself' class='col-sm-auto col-auto p-2 rounded'>";
    var html_end = "</div><div class='col'></div></div>";
    var html_myself_end = "</div></div>";
    var html_msgs = [];
    var msg_area = document.getElementById('msg_list');
    var log_key = chatlog_key;
    var sender;
    var content;
    firebase.database().ref('chatlog/'+log_key).on("child_added", function(s) {
        sender = s.val().sender;
        content = s.val().content;
        if(sender == user_name){
            html_msgs.push(html_myself_before_content + content + html_myself_end);
        }
        else{
            html_msgs.push(html_before_content + content + html_end);
        }
        msg_area.innerHTML = html_msgs.join("");
        msg_area.scrollTop = msg_area.scrollHeight;
    });
}

/* msg:
<div class="row w-100 px-2 mx-auto my-2">
    <div id="msg" class="col-sm-auto col-auto p-2 rounded bg-light">msg</div>
    <div class="col"></div>
</div> */

function draw_chatroom(roommate_name, type) {
    var html_before_title = "<div class='p-3 w-100' style='background-color:salmon;'><h4 id='room_title' class='my-auto'>";
    var html_after_title = "</h4></div><div id='msg_list' class='h-75 w-100'></div><div class='p-2 w-100 media' style='background-color:rgba(0,0,0,0.85);'><textarea id='txtMsg' class='media-body form-control' rows='3' autofocus style='resize: none;'></textarea><img id='btnSend' onclick='send_chatmsg()' src='img/send_64.png' class='my-auto ml-3 mr-2' alt='Send!'></div>";
    var html_public_after_title = "</h4></div><div id='msg_list' class='h-75 w-100'></div><div class='p-2 w-100 media' style='background-color:rgba(0,0,0,0.85);'><textarea id='txtMsg' class='media-body form-control' rows='3' autofocus style='resize: none;'></textarea><img id='btnSend' onclick='send_chatmsg_to_public()' src='img/send_64.png' class='my-auto ml-3 mr-2' alt='Send!'></div>";
    if(type == "private"){
        document.getElementById('chatroom_area').innerHTML = html_before_title+"與 "+roommate_name+" 的聊天"+html_after_title;
    }
    else if(type == "public"){
        document.getElementById('chatroom_area').innerHTML = html_before_title+"公開交流群"+html_public_after_title;
    }

}
var html_chatroom_alt = "<div id='chatroom_alt' class='h-100 w-100 d-flex justify-content-center'><div style='color:rgba(241, 241, 241, 0.8);font-size:30px;'>點擊右方使用者列表開啟一對一聊天室.</div></div>";

function open_public_room() {
    var html_before_sender = "<div class='row w-100 px-2 mx-auto my-2'><div id='msg' class='col-sm-auto col-auto p-2 rounded bg-light'><p id='public_room_msg_sender' class='my-auto'>";
    var html_after_sender = ":</p>";
    var html_after_content = "</div><div class='col'></div></div>";
    var html_myself_before_content = "<div class='row w-100 px-2 mx-auto my-2'><div class='col'></div><div id='msg_myself' class='col-sm-auto col-auto p-2 rounded'>";
    var html_myself_end = "</div></div>";
    var html_msgs = [];
    var msg_area;
    var sender;
    var content;
    var first_count = 0;
    var second_count = 0;

    firebase.database().ref('chatlog/'+chatlog_key).off();
    
    draw_chatroom("", "public");
    msg_area = document.getElementById('msg_list');
    
    var ref = firebase.database().ref('public');
    
    ref.once('value', function(snapshot) {
        if(!snapshot.exists()){
            ref.on('child_added', function(s) {
                sender = s.val().sender;
                content = s.val().content;
                if(sender == user_name){
                    html_msgs.push(html_myself_before_content + content + html_myself_end);
                }
                else{
                    html_msgs.push(html_before_sender + sender + html_after_sender + content + html_after_content);
                }
                msg_area.innerHTML = html_msgs.join("");
                msg_area.scrollTop = msg_area.scrollHeight;
            });
        }
        else{
            ref.once('value', function(snapshot) {
                snapshot.forEach(function(s) {
                    sender = s.val().sender;
                    content = s.val().content;
                    if(sender == user_name){
                        html_msgs.push(html_myself_before_content + content + html_myself_end);
                    }
                    else{
                        html_msgs.push(html_before_sender + sender + html_after_sender + content + html_after_content);
                    }
                    msg_area.innerHTML = html_msgs.join("");
                    msg_area.scrollTop = msg_area.scrollHeight;
                    first_count += 1;
                });

                ref.on('child_added', function(s) {
                    second_count += 1;
                    if(second_count > first_count){
                        sender = s.val().sender;
                        content = s.val().content;
                        if(sender == user_name){
                            html_msgs.push(html_myself_before_content + content + html_myself_end);
                        }
                        else{
                            html_msgs.push(html_before_sender + sender + html_after_sender + content + html_after_content);
                        }
                        msg_area.innerHTML = html_msgs.join("");
                        msg_area.scrollTop = msg_area.scrollHeight;
                        first_count += 1;
                    }
                });
            });
        }
    });
}

window.onload = function() {
    init();
}