function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignIn = document.getElementById('btnSignIn');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnGoogle = document.getElementById('btnGoogle');
    var error_msg = document.getElementById('error_msg');

    // firebase.auth().onAuthStateChanged(function(user) {
    //     if(user)
    //         document.location = "chatroom.html";
    // });

    btnSignUp.addEventListener('click', function() {
        document.location = "signup.html";
    });

    btnSignIn.addEventListener('click', function() {
        /// Add email login button event
        ///     1. Get user's email and password to login
        ///     2. Go to chatroom.html when login success
        ///     3. Show error message and clean the input fields
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function() {
            firebase.database().ref('users').once('value', function(snapshot) {
                snapshot.forEach(function(s) {
                    if(s.val().email == txtEmail.value){
                        user_key = s.key;
                        console.log(user_key);
                    }
                });
                firebase.database().ref('users/'+user_key+'/online').set(true).then(function() {
                    document.location = "chatroom.html";
                });
            });
        })
        .catch(function(error) {
            // Handle Errors here.
            error_msg.style.display = "block";
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    firebase.database().ref('users').limitToLast(1).once('child_added', function(snapshot){
        var num_regUsers = snapshot.key;
    });

    var provider = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function() {
        /// Add google login button event
        ///     1. Use popup function to login google
        ///     2. Go to chatroom.html when login success
        ///     3. Show error message
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            // What if had logged in before
            firebase.database().ref('users/'+user.email).set({
                name: user.displayName,
                email: user.email,
                online: true
            }).then(function() {document.location = "chatroom.html";});
            // document.location = "chatroom.html";
            })
            .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredentialtype that was used.
            var credential = error.credential;
            // create_alert("error", errorMessage);
        });
    });
}

// Custom alert
// function create_alert(type, message) {
//     var alertarea = document.getElementById('custom-alert');
//     if (type == "success") {
//         str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
//         alertarea.innerHTML = str_html;
//     } else if (type == "error") {
//         str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
//         alertarea.innerHTML = str_html;
//     }
// }

window.onload = function() {
    initApp();
};