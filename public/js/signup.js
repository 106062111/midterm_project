function init() {
    var btnSignUp = document.getElementById('btnSignUp');
    var txtUsername = document.getElementById('inputUsername');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var txtPasswordCheck = document.getElementById('inputPasswordCheck');
    var error_msg = document.getElementById('error_msg');

    var num_regUsers = 1;

    firebase.database().ref('users').on('child_added', function(snapshot) {
        name_used.push(snapshot.val().name);
    });

    txtPasswordCheck.addEventListener('change', function() {
        if(txtPassword.value != this.value){
            error_msg.innerText = "Password not match";
            error_msg.style.display = "block";
        }
        else{
            error_msg.innerText = "";
            error_msg.style.display = "none";
        }
    });
    txtPassword.addEventListener('change', function() {
        if(txtPasswordCheck.value != "" && txtPasswordCheck.value != this.value){
            error_msg.innerText = "Password not match";
            error_msg.style.display = "block";
        }
        else if(txtPasswordCheck.value != "" && txtPasswordCheck.value == this.value){
            error_msg.innerText = "";
            error_msg.style.display = "none";
        }
    });

    firebase.database().ref('users').limitToLast(1).once('child_added', function(snapshot){
        num_regUsers = snapshot.key;
    });

    firebase.auth().onAuthStateChanged(function(user) {
        if(user){
            if(user.displayName == null){
                user.updateProfile({
                displayName: txtUsername.value
                }).then(function() {
                // Update successful.
                    firebase.database().ref('users/'+`${num_regUsers-"0"+1}`).set({
                        email: txtEmail.value,
                        name: txtUsername.value,
                        online: true
                    }).then(function() {
                        console.log("update username: ok");
                        document.location = "chatroom.html";
                    });
                }).catch(function(error) {
                // An error happened.
                    console.log("update username: fail");
                });
            }
            else{
                document.location = "chatroom.html";
            }
        }
    });

    btnSignUp.addEventListener('click', function() {
        /// Add signup button event
        ///     1. Get user's email and password to signup
        ///     2. Show success message, sign in auto, and go to chatroom.html
        ///     3. Show error message
        if(error_msg.innerText == "Password not match")
            return;
        var inputready = true;
        if(txtUsername.value == "" || txtEmail.value == "" ||
        txtPassword.value == "" || txtPasswordCheck == ""){
            inputready = false;
            error_msg.innerText = "Please fill out the form!";
            error_msg.style.display = "block";
        }
        var invalidChars = "!@#$%^&*()<>?";
        var str_username = txtUsername.value;
        for(var i = 0; i < invalidChars.length; i++){
            var tmp = str_username.indexOf(invalidChars[i]);
            if(tmp != -1){
                inputready = false;
                error_msg.innerText = "Illegal characters in Username!";
                error_msg.style.display = "block";
                break;
            }
        }

        if(inputready){
            firebase.database().ref('users').once('value', function(snapshot) {
                snapshot.forEach(function(s) {
                    if(txtUsername.value == s.val().name){
                        error_msg.innerText = "Username already in use!";
                        error_msg.style.display = "block";
                        inputready = false;
                    }
                });
                if(inputready){
                    firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
                    .then(function() {
                        // create_alert("success", "");
                        // firebase.database().ref('users').limitToLast(1).once('child_added', function(snapshot) {
                            // num_regUsers = snapshot.key;
                            // firebase.database().ref('users/'+`${num_regUsers-"0"+1}`).set({
                            //     email: txtEmail.value,
                            //     name: txtUsername.value,
                            //     online: true
                            // })
                            // .then(function() {
                            //     setTimeout(function() {
                                    firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value);
                                // }, 1000);
                        //     });
                        // });
                    })
                    .catch(function(error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // create_alert("error", errorMessage);
                        switch(errorCode){
                            case "auth/email-already-in-use":
                                error_msg.innerText = "Email already in use!";
                                error_msg.style.display = "block";
                            break;
                            case "auth/invalid-email":
                                error_msg.innerText = "Wrong Email address format!";
                                error_msg.style.display = "block";
                            break;
                            case "auth/weak-password":
                                error_msg.innerText = "Password should be at least 6 characters!";
                                error_msg.style.display = "block";
                            break;
                            default:
                                error_msg.innerText = "Error signing up!";
                                error_msg.style.display = "block";
                            break;
                        }
                    });
                }
            });
        }
    });
}

window.onload = function() {
    init();
};